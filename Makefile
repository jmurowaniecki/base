LIBRARY = docker/λ
SERVICE = bash $(LIBRARY)

.DEFAULT: help

help: $(LIBRARY)
	$(SERVICE) help

start:
	$(SERVICE) up -d

debug:
	$(SERVICE) up

stop:
	$(SERVICE) down

install:
	$(SERVICE) install
