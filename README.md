# Agnostic PHP Boilerplate

![Codename: FIAT LUX](https://img.shields.io/badge/codename-fiat_lux-blue.svg)
![Version: pre-alpha](https://img.shields.io/badge/version-pre_alpha-lightgrey.svg)

Utilize como padrão para iniciar seus projetos de forma rápida e descomplicada com **Composer** e **Docker**.

# Estrutura

Sugestão de subdivisão para pastas **data**, **docker**, **documentation** e **service-names-etc** de forma a facilitar o gerenciamento e organização de questões pertinentes ao código (que deverão ser implementadas em *service-etc*), a documentação (bem como levantamento de requisitos, wiki, configurações, etc - em *documentation*) enquanto as pastas *data* e *docker* deverão conter dumps, logs, e configurações de containers Docker, automatizadores de gerenciamento de ambiente, entre outros.

Diretórios/arquivos|Função ou conteúdo
---|---
**docker** | Arquivos de configuração dos containers criados pelo **docker-compose.yml**, mensagem de abertura e configuração dos serviços.
**service-\*** | Projeto(s) em execução.
Makefile | Arquivo de montagem.
docker-compose.yml | Estrutura dos Docker containers.
.editorconfig | Padrões para arquivos do projeto.
phpcs.xml | Coleção de regras para assegurar PHP Coding Standart.
composer.json | Identificador do projeto - **MODIFICAR APÓS INSTALAÇÃO**.

## Guia de Instalação

Execute o comando `composer -sdev create-project compilou/base nome_do_seuprojeto` para criar um projeto base na pasta *nome_do_seuprojeto*.

> Lembrando que a opção `-sdev` deve-se apenas para permitir a criação do projeto com a versão mais recente em desenvolvimento.

Após adquirir o projeto, execute o comando `make install` dentro da pasta do projeto para executar as rotinas de instalação e configuração, que resultará em uma saída conforme exemplo abaixo:

```
λ@fiatlux:~$ composer create-project compilou/base projeto-teste
λ@fiatlux:~$ cd projeto-teste
λ@fiatlux:~/projeto-teste$ make install

docker-compose build

...
... Relatório extenso do build do Docker Compose.
...
... ATENÇÃO:
...          Esse processo pode demorar um pouco.
...

Removing intermediate container e79886545c15
Successfully built 30bbf113f2d5
Successfully tagged basephp_php7:latest
mariadbdata uses an image, skipping
mariadb uses an image, skipping

λ@fiatlux:~/projeto-teste$
```

> **Dica:** O comando `make install` executa o build dos containers e, logo a seguir, instala as dependências via `composer` e `npm`. Para mais informações sobre as opções de montagem e automação execute o comando `make` sem parâmetros.


## Primeiros passos

Utilize o comando `make start` para inicializar o projeto e `make stop` para finalizar os containers. A execução do comando `make start` exibirá uma saída similar ao exemplo abaixo:

```
λ@fiatlux:~/projeto-teste$ make start

docker-compose up -d
Creating network "basephp_pipeline" with the default driver
Creating basephp_mariadbdata_1
Creating basephp_php7_1
Creating basephp_mariadb_1

λ@fiatlux:~/projeto-teste$
```
**Pronto!** Seu projeto já está disponível e pode ser acessado pelo endereço http://localhost/

> **Dica:** se você tiver outros containers utilizando as portas padrão do Apache/NGinx/HTTPd e MariaDB/MySQL os serviços não serão inicializados. Para corrigir isso será necessário alterar essas portas para outras que estejam livres.


# Colabore

Envie sua sugestão abrindo uma issue, comentando ou realizando um pull request.
