<?php

$info = [
    'get_loaded_extensions' => get_loaded_extensions(),
    'apache_get_modules'    => apache_get_modules(),
    'ini_get_all'           => ini_get_all(),
    'phpversion'            => phpversion(),
    'php_uname'             => php_uname(),
];

/**
 * @param Array  $array          Collection of elements.
 * @param String $separator      character separator.
 * @param String $last_separator Character separator.
 *
 * @return String
 */
$printArray = function ($array, $separator = ', ', $last_separator = ' and ') {
    if (count($array) < 2) {
        $last_separator = null;
    }
    $last_element = array_pop($array);
    return implode($separator, $array) . "{$last_separator}{$last_element}";
};

?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Agnostic PHP Boilerplate</title>
    <link rel="stylesheet" href="/assets/styles.min.css">
</head>

<body>
    <section class="header">
        <h1>Agnostic PHP Boilerplate</h1>
        <h2>container image version: <?php echo $info['phpversion'] ?></h2>
    </section>

    <section class="body">
        <h3><?php echo $info['php_uname'] ?></h3>
        <p><b>Loaded PHP extentions</b>: <?php echo $printArray($info['get_loaded_extensions']) ?>.</p>
        <p><b>Loaded Apache Modules</b>: <?php echo $printArray($info['apache_get_modules']) ?>.</p>
        <hr>
        <p>Using <a href="http://milligram.io/" target="_blank">http://milligram.io/</a> and <a href="http://fontawesome.io/" target="_blank">http://fontawesome.io/</a>.</p>
    </section>

    <section class="footer">
        <ul>
            <li><a target="_blank" href="https://gitlab.com/compilou" class="fa fa-gitlab"></a></li>
            <li><a target="_blank" href="https://github.com/compilouit" class="fa fa-github"></a></li>
            <li><a target="_blank" href="https://twitter.com/compilouit" class="fa fa-twitter"></a></li>
            <li><a target="_blank" href="https://facebook.com/compilouit" class="fa fa-facebook"></a></li>
            <li><a target="_blank" href="mailto:support@compilou.com.br" class="fa fa-envelope"></a></li>
        </ul>
        <p>CompilouIT 2017 - alguns direitos reservados.</p>
    </section>
</body>

</html>
